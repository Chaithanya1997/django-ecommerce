from django.contrib import admin
from .models import Customer, Product, OrderPlaced, Cart
# Register your models here.

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'name']


@admin.register(Product)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'selling_price', 'brand']


@admin.register(OrderPlaced)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'product', 'customer', 'quantity']


@admin.register(Cart)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'product', 'quantity']


