from django.shortcuts import render
from django.views import View
from .models import Customer, Product


# def home(request):
#  return render(request, 'app/home.html')

class ProductView(View):
    def get(self, request):
        topwear = Product.objects.filter(category='TW')
        btwear = Product.objects.filter(category='BW')
        mobile = Product.objects.filter(category='M')
        context1 = {'topwear':topwear, 'btwear':btwear, 'mobile':mobile}
        return render(request, 'app/home.html', context1)

# def product_detail(request):
#  return render(request, 'app/productdetail.html')

class Product_detail(View):
    def get(self, request, pk):
        product = Product.objects.get(pk=pk)
        print(product)
        context1 = {'product':product}
        return render(request, 'app/productdetail.html', context1)


def add_to_cart(request):
 return render(request, 'app/addtocart.html')

def buy_now(request):
 return render(request, 'app/buynow.html')

def profile(request):
 return render(request, 'app/profile.html')

def address(request):
 return render(request, 'app/address.html')

def orders(request):
 return render(request, 'app/orders.html')

def change_password(request):
 return render(request, 'app/changepassword.html')

def mobile(request, data=None):
    if data == None:
        mobiles = Product.objects.filter(category='M')
    elif data == 'Redmi' or data == 'Samsung':
        mobiles = Product.objects.filter(category='M').filter(brand = data)
    elif data == 'below':
        mobiles = Product.objects.filter(category='M').filter(discount_price__lt = 10000)
    elif data == 'above':
        mobiles = Product.objects.filter(category='M').filter(discount_price__gt = 10000)
    context = {'mobiles':mobiles}
    return render(request, 'app/mobile.html', context)

def top_wear(request, data=None):
    if data == None:
        dress = Product.objects.filter(category='TW')
    elif data == 'above':
        dress = Product.objects.filter(category='TW').filter(discount_price__gt = 500)
    elif data == 'below':
        dress = Product.objects.filter(category='TW').filter(discount_price__lt = 500)
    context = {'dress':dress}
    return render(request, 'app/top_wear.html', context)

def login(request):
 return render(request, 'app/login.html')

def customerregistration(request):
 return render(request, 'app/customerregistration.html')

def checkout(request):
 return render(request, 'app/checkout.html')
