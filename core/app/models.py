from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator 
# Create your models here.

class Customer(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE, null=True)
    name = models.CharField(max_length = 100, null= True)
    locality = models.CharField(max_length = 100, null= True)
    city = models.CharField(max_length = 100, null= True)
    zipcode = models.IntegerField(null= True)    
    state = models.CharField(max_length = 100, null= True)
    def __str__(self):
        return str(self.id)
    
CAT = (
    ('M', 'Mobile'),
    ('L', 'Laptop'),
    ('TW', 'Top wear'),
    ('BW', 'Bottom Wear')
)

class Product(models.Model):
    title = models.CharField(max_length = 100, null= True)
    selling_price = models.FloatField(null= True)    
    discount_price = models.FloatField(null= True)    
    discription = models.TextField(null= True)    
    brand = models.CharField(max_length = 100, null= True)
    category = models.CharField(choices = CAT, max_length = 2, null= True)
    product_image = models.ImageField(upload_to='uploads/product', null=True)
    def __str__(self):
        return str(self.id)

STATUS = (
    (1, 'Accepted'),
    (2, 'Packed'),
    (3, 'On the Way'),
    (4, 'Delivery'),
    (5, 'Cancel'),
)

class OrderPlaced(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE, null=True)
    customer = models.ForeignKey(Customer, on_delete = models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete = models.CASCADE, null=True)
    quantity = models.PositiveIntegerField(default = 1, null=True)
    orderd_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default = 1, null= True)
    def __str__(self):
        return str(self.id)

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete = models.CASCADE, null=True)
    quantity = models.PositiveIntegerField(default = 1, null=True)
    def __str__(self):
        return str(self.id)

